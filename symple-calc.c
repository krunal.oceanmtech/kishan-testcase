#include "header.h"
#include <stdio.h>

void calc() {
  // getting first number
  printf("Enter first number: ");
  int first;
  scanf("%d", &first);
  // getting second number
  printf("Enter second number: ");
  int second;
  scanf("%d", &second);

  // getting operator
  printf("Enter operator: ");
  char operator;
  scanf(" %c", &operator);

  // switch operator
  switch (operator) {
  case '+':
    printf("%d + %d = %d\n", first, second, first + second);
    break;
  case '-':
    printf("%d - %d = %d\n", first, second, first - second);
    break;
  case '*':
    printf("%d * %d = %d\n", first, second, first * second);
    break;
  case '/':
    printf("%d / %d = %d\n", first, second, first / second);
    break;
  default:
    printf("Invalid operator\n");
    break;
  }
}
