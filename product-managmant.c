#include "header.h"
#include <stdio.h>

// create a structure that will store id name price and quantity of the product
struct product {
  int id;
  char name[20];
  float price;
  int quantity;
};

void man() {
  // declare an array of structure
  struct product p[5];
  // declare a variable to store the number of products
  int n;
  // ask the user to enter the number of products
  printf("Enter the number of products: ");
  scanf("%d", &n);
  // loop to store the details of the products
  for (int i = 0; i < n; i++) {
    printf("Enter the details of product %d: \n", i + 1);
    printf("Enter the id: ");
    scanf("%d", &p[i].id);
    printf("Enter the name: ");
    scanf("%s", p[i].name);
    printf("Enter the price: ");
    scanf("%f", &p[i].price);
    printf("Enter the quantity: ");
    scanf("%d", &p[i].quantity);
  }
  // print the details of the products
  printf("\nThe details of the products are: \n");
  for (int i = 0; i < n; i++) {
    printf("\nProduct %d: \n", i + 1);
    printf("Id: %d\n", p[i].id);
    printf("Name: %s\n", p[i].name);
    printf("Price: %.2f\n", p[i].price);
    printf("Quantity: %d\n", p[i].quantity);
  }
}
